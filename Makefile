prefix=/usr/local
bindir=$(prefix)/bin

RCDIRS = /etc/Muttrc.d /etc/neomuttrc.d

all:
	@echo "Available targets:"
	@echo " make install"
	@echo " make install-systemwide-rc"

install:
	install -m 755 cartman-mutt $(bindir)
	@echo
	@echo "Try also:"
	@echo "make install-systemwide-rc"
	@echo

install-systemwide-rc:
	@found=0; for d in $(RCDIRS); do \
	  if test -d $$d; then \
      echo "Installing cartman-mutt.rc in $$d"; \
      found=$$((found+1)); \
      install -m 644 cartman-mutt.rc $$d; \
    fi; \
  done; if [ $$found = 0 ]; then \
    echo "There's none of $(RCDIRS) directories," \
         "dunno how to install systemwide"; \
  fi
